

var Basket = React.createClass({

    getInitialState: function(){
        return {
            count: 1
        }
    },

    handleClickPlus: function(){
        if(this.state.count < this.props.max)
            this.setState({count: this.state.count+1})
    },

    handleClickMinus: function(){
        if(this.state.count > this.props.min)
            this.setState({count: this.state.count-1})
    },

    handleOnChange: function(e){
        var val = e.target.value
        if(val < this.props.min && val > this.props.max)
            val =
        this.setState({count: e.target.value})
    },

    render: function(){
        return React.DOM.div({},[
            React.DOM.button({onClick: this.handleClickMinus}, "-"),
            React.DOM.input({type: "text", name: this.props.id, value: this.state.count, onChange: this.handleOnChange}),
            React.DOM.button({onClick: this.handleClickPlus}, "+"),
        ])

    }
})

React.renderComponent(
    Basket({min: 0,  max: 10,  id: "myInput-3"}),
    document.body
)
