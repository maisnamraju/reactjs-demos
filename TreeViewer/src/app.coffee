

TreeViewer = React.createClass
    displayName: "Tree"
    render:()->
        React.DOM.ul {},
            (TreeFolder({data: @props.tree.children, name: @props.tree.name}))


TreeFolder = React.createClass
    displayName: "Folder"
    getInitialState: ()->
        {open: false}
    onOpenClick: ()->
        @setState {open: !@state.open}

    getContent: ()->
        @props.data.map (item)->
            if item.type == "folder"
                TreeFolder({data: item.children, name: item.name})
            else if item.type == "file"
                TreeFile({data: item})

    render: ()->
        React.DOM.li {}, [
            React.DOM.span {onClick: @onOpenClick}, "[folder] " + @props.name + if(@state.open) then "[-]" else "[+]"
            React.DOM.ul {}, if @state.open then @getContent()
        ]



TreeFile = React.createClass
    displayName: "File"
    render:()->
        React.DOM.li {}, "[file] " + @props.data.name




treeData = {
    name: 'My Tree',
    type: "folder",
    children: [
        { type: 'file', name: 'hello1' },
        { type: 'file', name: 'wat' },
        {
            type: 'folder',
            name: 'child folder',
            children: [
                {
                    type: 'folder',
                    name: 'child folder',
                    children: [
                        { type: 'file', name: 'hello2' },
                        { type: 'file', name: 'wat' }
                    ]
                },
                { type: 'file', name: 'hello' },
                { type: 'file', name: 'wat' },
                {
                    type: 'folder',
                    name: 'child folder',
                    children: [
                        { type: 'file', name: 'hello3' },
                        { type: 'file', name: 'wat' }
                    ]
                }
            ]
        }
    ]
}


React.renderComponent(TreeViewer({tree: treeData}), document.getElementById("container"))